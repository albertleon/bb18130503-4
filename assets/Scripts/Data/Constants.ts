import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

enum EventName{
    Restart_Game = 'restart',
    FINISHED_WALK = 'finished-walk',
    START_BRAKING = 'start-braking',
    END_BRAKING = 'end-braking',
    SHOW_COIN = 'show-coin',
    GAME_START = 'game-start',
    GAME_OVER = 'game-over',
    NEW_LEVEL = 'new-level',
    SHOW_TALK = 'show-talk',
    SHOW_GUIDE = 'show-guide',
    UPDATE_PROGRESS = 'update-progress',
}

enum AudioSource{
    BACKGROUND = 'background',
    CLICK = 'click',
    CRASH = 'crash',
    GETMONEY = 'getMoney',
    INCAR = 'inCar',
    NEWORDER = 'newOrder',
    START = 'start',
    STOP = 'stop',
    TOOTING1 = 'tooting1',
    TOOTING2 = 'tooting2',
    WIN = 'win'
}


@ccclass('Constants')
export class Constants{
    public static EventName = EventName;
    public static AudioSource = AudioSource;

    //UI界面名称的枚举.
    public static UIPage = {
        mainPanel:'MainPanel',
        gamePanel:'GamePanel',
        failPanel:'FailPanel',
        winPanel:'WinPanel',
    }

    public static GameConfigID = 'BBI_GAME_CACHE';
    public static PlayerConfigID = 'playerInfo';
    public static MaxLevel = 2;
}
