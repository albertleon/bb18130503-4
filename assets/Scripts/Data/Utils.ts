import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

/**  工具类，包含一些数学函数     */

@ccclass('Utils')
export class Utils{
    //根据最小值和最大值，求一个二者之间的随机值.
    public static getRandomNumber(min: number, max: number) {
        //Math.random()得到的是0-1之间的一个随机数，可能随机到0，不可能随机到1.
        const range = max - min;
        let randomValue = Math.random() * range;
        randomValue += min;
        return randomValue;
    }
}
