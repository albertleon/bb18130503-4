import { _decorator, Component, Node, ProgressBarComponent } from 'cc';
import { Constants } from '../Data/Constants';
import { UIManager } from './UIManager';
const { ccclass, property } = _decorator;

/**     游戏战斗界面控制程序      */

@ccclass('GamePanel')
export class GamePanel extends Component {
    @property({
        type:ProgressBarComponent,
        displayOrder:1,
    })
    //关卡进度条组件.
    progressBar:ProgressBarComponent = null;

    //通关时间，暂时写死30秒.
    private _levelMaxTime = 30;
    //当前计时的时间.
    private _curCountTime = 0;
    //是否开始计时.
    private _isStart = false;

    //窗口被打开时执行的函数.
    public show(){
        this._curCountTime = 0;
        this._isStart = true;
        this.progressBar.progress = 0;
    }

    update(dt:number){
        if(this._isStart){
            //dt就是每一帧运行所需的时间.
            this._curCountTime += dt;
            this.progressBar.progress = this._curCountTime/this._levelMaxTime;
            //判断计时是否结束.
            if(this._curCountTime>this._levelMaxTime){
                this._isStart = false;
                //游戏结束，关闭GamePanel，弹出胜利界面.
                UIManager.hideDialog(Constants.UIPage.gamePanel);
                UIManager.showDialog(Constants.UIPage.winPanel,null);
            }
        }
    }
}
