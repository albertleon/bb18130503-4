import { _decorator, Component, Node, ButtonComponent } from 'cc';
import { Constants } from '../Data/Constants';
import { CustomEventListener } from '../Data/CustomEventListener';
import { BattleManager } from '../Game/BattleManager';
import { UIManager } from './UIManager';
const { ccclass, property } = _decorator;

/**    失败界面控制程序      */

@ccclass('FailPanel')
export class FailPanel extends Component {
    @property({
        type:ButtonComponent,
        displayOrder:1,
    })
    //重新开始按钮.
    btn_restart:ButtonComponent = null;

    onLoad(){
        //注册重新开始游戏按钮的点击事件及其响应函数.
        this.btn_restart.node.on('click',this._restartBtnClick,this);
    }

    private _restartBtnClick(){
        //回到主界面.
        UIManager.hideDialog(Constants.UIPage.failPanel);
        CustomEventListener.dispatchEvent(Constants.EventName.Restart_Game);
    }
}
