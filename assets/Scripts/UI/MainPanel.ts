import { _decorator, Component, Node, ButtonComponent, AnimationComponent } from 'cc';
import { Constants } from '../Data/Constants';
import { CustomEventListener } from '../Data/CustomEventListener';
import { BattleManager } from '../Game/BattleManager';
import { UIManager } from './UIManager';
const { ccclass, property } = _decorator;

/**     主界面的控制程序    */

@ccclass('MainPanel')
export class MainPanel extends Component {
    @property({
        type:ButtonComponent,
        displayOrder:1,
    })
    //开始游戏按钮.
    btn_startGame:ButtonComponent = null;

    onLoad(){
        //注册开始游戏按钮的点击事件及其响应函数.
        this.btn_startGame.node.on('click',this._startGameBtnClick,this);
    }

    //开始游戏按钮点击响应函数.
    private _startGameBtnClick(){
        //发布了游戏开始事件.
        CustomEventListener.dispatchEvent(Constants.EventName.GAME_START);
        //马上关闭主界面.
        UIManager.hideDialog(Constants.UIPage.mainPanel,null);
        //马上打开战斗界面GamePanel.
        UIManager.showDialog(Constants.UIPage.gamePanel,null);
    }
}
