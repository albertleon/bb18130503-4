import { _decorator, Component, Node, Vec3, tween, Tween } from 'cc';
import { Monster } from './Monster';
const { ccclass, property } = _decorator;

/**   怪物的视图类，用于控制怪物的表现和动画   */

@ccclass('MonsterView')
export class MonsterView extends Component {
    @property({
        type:Boolean,
        displayOrder:1
    })
    //是否是飞行类怪物，true表示是飞行类.
    isFlyMonster:boolean = false;

    //怪物的tween动画实例.
    private _tween:Tween = null;
    private _owner:Monster = null;

    public init(owner:Monster){
        this._owner = owner;
    }

    //移动到target参数指定的坐标点，耗时由参数time决定.
    public moveToTarget(target:Vec3,time:number,cb:Function){
        this.stopMove();
        //使用tween，实现移动到具体目标点的功能.
        this._tween = tween(this.node)
            .to(time, { position: target })
            .call(() => {
                //回调函数，到达目标点后.
                if(cb!=null){
                    cb();
                }
            })
            .start();
    }

    //停止怪物的移动动画.
    public stopMove(){
        if(this._tween){
            this._tween.stop();
            this._tween = null;
        }
    }

    public dispose(){
        this.node.destroy();
    }

    public onHurt(){
        this._owner.onHurt();
        this.dispose();
    }
}
