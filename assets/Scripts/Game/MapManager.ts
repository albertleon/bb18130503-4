import { _decorator, Component, Node, Vec3 } from 'cc';
const { ccclass, property } = _decorator;

/*******     此代码用于地形的移动和动态加载    *********/

@ccclass('MapManager')
export class MapManager extends Component {
    @property({
        type:[Node],
        displayOrder:1
    })
    //4块地形对应的数组.
    terrainPrefabArray:Node[] = [];
    
    //用于任何物体移动的中间变量，用于存储物体的坐标.
    private _offset:Vec3 = new Vec3();
    private _speed = 10;

    update(dt:number){
        for (let i = 0; i < this.terrainPrefabArray.length; i++) {
            const element = this.terrainPrefabArray[i];
            //下面是关于移动部分的标准代码.

            //将节点的坐标取出，暂时存在中间变量_offset中.
            this._offset.set(element.worldPosition);
            //将该坐标加上速度乘以时间得到的距离,_offset中就保存了一个新的坐标.
            this._offset.add(new Vec3(0,0,-this._speed*dt));
            //再重新将_offset赋值回物体的坐标中去，让物体的坐标发生改变.
            element.setWorldPosition(this._offset);
            if(element.worldPosition.z<-300){
                const z = element.worldPosition.z;
                //将数组中最后一个元素，即最后一块地板的坐标，加400，使之提升到最前面.
                //如果，最远处的地板的z坐标小于-300，则将最远处的地板移动到最近处(z坐标为100)，该地板成为最近处地板.
                element.setWorldPosition(new Vec3(0,0,z+400));
            }
        }
    }
}
