import { _decorator, Component, Node, Vec3, ColliderComponent, ITriggerEvent } from 'cc';
import { MonsterView } from './MonsterView';
const { ccclass, property } = _decorator;

/**    子弹的控制类和视图类的结合体，即子弹创建后的行为控制     */

@ccclass('BulletView')
export class BulletView extends Component {

    //子弹的飞行方向.
    private _flyDir:Vec3 = new Vec3();
    //用于任何物体移动的中间变量，用于存储物体的坐标.
    private _offset:Vec3 = new Vec3();
    private _speed = 15;


    //point2Pos是子弹弹道的的第二点坐标.
    public init(point2Pos){
        //两个坐标相减，得到子弹的飞行方向.
        Vec3.subtract(this._flyDir,point2Pos,this.node.worldPosition);
        //将方向归一化.
        this._flyDir = this._flyDir.normalize();
        let collider = this.getComponent(ColliderComponent);
        collider.on('onTriggerEnter', this.onTrigger, this);
    }

    //子弹飞行程序.
    update(dt:number){
        //下面是关于移动部分的标准代码.

        //将节点的坐标取出，暂时存在中间变量_offset中.
        this._offset.set(this.node.worldPosition);
        //delta是子弹位置变化量，它由子弹的飞行方向乘以距离来决定.
        let delta = new Vec3();
        Vec3.multiplyScalar(delta,this._flyDir,(this._speed*dt));
        //将该坐标加上速度乘以时间得到的距离,_offset中就保存了一个新的坐标.
        this._offset.add(delta);
        //再重新将_offset赋值回物体的坐标中去，让物体的坐标发生改变.
        this.node.setWorldPosition(this._offset);
        if(this.node.worldPosition.z<-300){
            //回收子弹.
            this._dispose();
        }
    }

    private _dispose(){
        this.node.destroy();
    }

    //如果子弹和某个碰撞器相撞.
    private onTrigger(event: ITriggerEvent){
        //otherCollider就是子弹碰到的另一个物体的碰撞器.
        const otherCollider = event.otherCollider;
        let view = otherCollider.getComponent(MonsterView);
        //如果被碰到的物体身上，有MonsterView这个组件，就表示，子弹碰到了敌人.
        if(view){
            view.onHurt();
        }
    }
}
