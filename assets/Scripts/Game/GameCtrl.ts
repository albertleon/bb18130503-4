import { _decorator, Component, Node, EventTouch, CameraComponent, geometry, PhysicsSystem, Vec3 } from 'cc';
import { Constants } from '../Data/Constants';
import { CustomEventListener } from '../Data/CustomEventListener';
import { UIManager } from '../UI/UIManager';
import { BattleManager } from './BattleManager';
const { ccclass, property } = _decorator;

/**     游戏的控制类，比如鼠标点击行为      */

@ccclass('GameCtrl')
export class GameCtrl extends Component {
    @property({
        type:CameraComponent,
        displayOrder:1,
    })
    //摄像机组件.
    cameraComp:CameraComponent = null;

    @property({
        type:Node,
        displayOrder:2,
    })
    //摄像机的远平面的模拟.
    cameraPanel:Node = null;

    start(){
        //鼠标点击屏幕事件的注册.
        this.node.on(Node.EventType.TOUCH_START,this._touchStart,this);
        //注册游戏开始事件.
        CustomEventListener.on(Constants.EventName.GAME_START,this._startGame,this);
        CustomEventListener.on(Constants.EventName.Restart_Game,this._restartGame,this);
        //打开主界面窗口.
        UIManager.showDialog(Constants.UIPage.mainPanel,null);
        BattleManager.instance().init();
    }

    //定义一个射线对象，用于从摄像机开始，沿着手指指向屏幕的方向，射向3D场景内.
    private _ray: geometry.ray = new geometry.ray();

    private _touchStart(event:EventTouch){
        //将手指或鼠标点击的位置(二维坐标)，转化为从摄像机发出的，指向屏幕上该点的一根射线.
        this.cameraComp.screenPointToRay(event.getLocationX(), event.getLocationY(), this._ray);
        
        //基于物理碰撞器的射线检测,raycast函数将射线向3D场景发射，此过程中检测沿途所有具有碰撞器的物体.
        if (PhysicsSystem.instance.raycast(this._ray)) {
            //射线检测到的沿途所有有碰撞器的物体的信息，保存在raycastResults里.
            const r = PhysicsSystem.instance.raycastResults;
            for (let i = 0; i < r.length; i++) {
                const item = r[i];
                //uuid表示一个物体的唯一id.
                if (item.collider.node.uuid == this.cameraPanel.uuid) {
                    BattleManager.instance().fire(item.hitPoint);
                    return;
                }
            }
        }
    }

    //进入战斗，主界面点击开始游戏的响应函数.
    private _startGame(){
        //让摄像机从第三人称切换到第一人称.
        BattleManager.instance().camAnimationComp.play('camMove');
        BattleManager.instance().gameStart();
    }

    //回到主界面，显示的仍然是之前关卡的内容(从失败界面回来).
    private _restartGame(){
        //回收所有的怪物.
        BattleManager.instance().recycleMonsters();
        BattleManager.instance().camAnimationComp.stop();
        this.cameraComp.node.setWorldPosition(new Vec3(0,10,18));
        this.cameraComp.node.setWorldRotationFromEuler(-10,0,0);
        //打开主界面窗口.
        UIManager.showDialog(Constants.UIPage.mainPanel,null);
        BattleManager.instance().init();

    }

    //回到主界面，但是显示的是新的关卡的内容(从胜利界面回来主界面).
    private _newLevel(){

    }
}
